pSTICK
======

pSTICK is a "stepstick" replacement that instead of directly driving
stepper motors, drives external stepper drivers through the typical
A1/A2/B1/B2 stepper motor pins on a stepper board.  This is intended
for external stepper drivers that require more current than can be
safely or correctly supplied by the STEP, DIR, and ~ENABLE pins on
a board.  It is essentially a logic level "signal booster" that can
boost both voltage (1.8V or 3.3V to 5V) and current (up to 150mA
total, or lower limit of whatever SOT-23 LDO you choose).

This uses p-MOSFET high-side switches, so it can be used to drive
non-isolated steppers with Ground / Direction / Step / Enable that
require more current than provided by a board. It can also drive
opto-isolated drivers by tying ground together between the channels
on the external stepper driver, and connecting the positive signal
side on the driver to their respective pins.

This design uses Vmot as the power source for the boosted signals.
It **will not tolerate 24V Vmot; it is designed for 6-12V Vmot only.**
If you provide this with 24V, the voltage regulator may catch fire,
even though a fuse is provided to make this failure less likely.
There is no need to provide 24V Vmot to boards using this driver
board, since this board moves the actual motor supply to external
drivers.

Credit
======

Bart Dring provided the idea and very helpful review of this
design.

Intellectual Property
=====================

Copyright Michael K Johnson

Licensed under CERN OHL v.1.2

This documentation describes Open Hardware and is licensed under the
CERN OHL v. 1.2.

You may redistribute and modify this documentation under the terms
of the CERN OHL v.1.2. (http://ohwr.org/cernohl). This documentation
is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF
MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR
PURPOSE. Please see the CERN OHL v.1.2 for applicable conditions
